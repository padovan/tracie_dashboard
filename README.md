Tracie dashboard
================

A web dashboard for the results of the tracie trace-based Mesa CI.

The dashboard is built using Django.

Get dashboard
-------------

    $ git clone https://gitlab.freedesktop.org/gfx-ci/tracie/tracie_dashboard

Development setup
-----------------

### Activate and prepare virtualenv

    $ python3 -m venv tracie-dashboard-venv
    $ source tracie-dashboard-venv/bin/activate
    $ pip install -r tracie_dashboard/requirements.txt

Instructions below assume that the virtualenv is active.

### Prepare dashboard

Copy `tracie_dashboard/setup-env.sh.sample` to `tracie_dashboard/setup-env.sh`
and update the variables with sensible values for your setup:

You can get a django secret key with:

    $ python3 -c 'from django.core.management.utils import get_random_secret_key; print(get_random_secret_key());'

A gitlab access token can be generated from your gitlab.freedesktop.org
profile, in the "Personal Access Token" section. The gitlab access token must
be created with the "api" scope.

In tracie_dashboard do:

    tracie_dashboard$ python3 manage.py makemigrations
    tracie_dashboard$ python3 manage.py migrate

### Run dashboard

First ensure that all required environment variables are in scope:

    tracie_dashboard$ source setup-env.sh

For a development run of the dashboard (note that first run may take some time
while data is downloaded from gitlab.freedesktop.org and cached):

    tracie_dashboard$ python3 manage.py runserver

Run the tests with:

    tracie_dashboard$ python3 manage.py test

Using docker
------------

A `Dockerfile` is provided for creating an instance of the dashboard powered by
uwsgi and nginx. To create the docker image run:

    tracie_dashboard$ docker build --tag tracie_dashboard .

To run the latest built image a convenience script is provided:

    tracie_dashboard$ ./start-docker.sh [--host-port=PORT] [--docker-image=IMAGE]

The default values for PORT and IMAGE are '80' and 'tracie_dashboard:latest',
respectively.

Note that `start-docker.sh` depends on properly populated `setup-env.sh`.
